const counterFactory = () => {

    let count = 0;
    return {
        increment: () => (++count),
        decrement: () => (--count),
    };
};

module.exports = {counterFactory};