const limitFunctionCallCount = (cb, n) => {

    let callCount = 0;
    return (...args) => {
        if (callCount === n) return null;
        callCount++;
        return cb(...args);
    };
};

module.exports = {limitFunctionCallCount};