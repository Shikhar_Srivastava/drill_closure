function cacheFunction(cb) {

    const cache = {};
    return (input) => {
        if (Object.prototype.hasOwnProperty.call(cache, input)) {console.log("fetched from cache");return cache[input]};
        cache[input] = cb(input);
        return cache[input];
    };
};

module.exports = {cacheFunction};
