const {counterFactory} = require('../counterFactory.js')

const count = counterFactory()

console.log(count.increment())
console.log(count.increment())
console.log(count.increment())

console.log(count.increment())
console.log(count.decrement())
