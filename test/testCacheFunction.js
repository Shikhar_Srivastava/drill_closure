const {cacheFunction} = require('../cacheFunction.js')
const cache = cacheFunction(foo)

console.log(cache(5));
console.log(cache(5));
console.log(cache(5));
console.log(cache(50));